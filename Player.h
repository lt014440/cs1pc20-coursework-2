#include <iostream>
#include <vector>
#include "Room.h"
#include "Character.h"
#include "Quest.h"
#pragma once

class Player : public Character { // class = player, subclass of character
private:
    Room* location;
public:
    Player(const string& name, int health);
    void SetLocation(Room* room);
    Room* GetLocation();
    void move(const string& command);
    void PickupItem(string& item);
    void InspectItem(string& item);
    vector<Quest*> currentQuests;
};