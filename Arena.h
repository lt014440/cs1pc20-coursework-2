#include <iostream>
#include "Room.h"
#pragma once

using namespace std;

class Arena
{
	private:
		map<string, Room*> rooms;
	public:
		void AddRoom(string& name, Room* room);
		Room* GetRoom(const string name);
		void ConnectRooms(const string room1, const string room2, const string direction);
		void LoadMapFromFile(const string& filename, int& roomsCounter);
		string getOppositeDirection(const string& direction);
};

