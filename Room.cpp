#include <iostream>
#include <vector>
#include <map>
#include "Item.h"
#include "Room.h"
#pragma once

using namespace std;

string Room::GetDescription() {
        return desc;
    }
Room::Room(const string& setdesc) {
    desc = setdesc;
}
void Room::AddItem(const Item& item) {
    items.push_back(item);
}
void Room::RemoveItem(string item) {
    int itemIndex = 0;
    Item::itemTypes itemType = Item::box;

    if (item == "key") {
        itemType = Item::key;
    }
    else if (item == "box") {
        itemType = Item::box;
    }

    auto i = Room::items.begin();
    while (i != Room::items.end()) {
        string newName = "";
        for (int x = 0; x < i->GetName().size(); x++) {
            newName += tolower(i->GetName()[x]);
        }
        if (i->itemType == itemType || newName == item) {
            i = Room::items.erase(i);
            break;
        }
        else
            ++i;
    }
}
void Room::AddExit(string direction, const Room* room) {
    exits[direction] = room;
}
bool Room::HasExit(const string& direction) const {
    return exits.find(direction) != exits.end();
}
Room* Room::GetExit(const string& direction) const {
    auto it = exits.find(direction);
    if (it != exits.end()) {
        return (Room*)it->second;
    }
    return nullptr; // No exit found in the specified direction
}
vector<Item> Room::GetItems() {
    return items;
}
bool Room::IsLocked() {
    return Room::isLocked;
}
void Room::ChangeDoorLock(bool condition) {
    isLocked = condition;
}
bool Room::CheckPass(string guess) {
    if (guess == pass) {
        return true;
    }
    else
        return false;
}
void Room::SetPass(string password) {
    pass = password;
}
string Room::GetPass() {
    return pass;
}