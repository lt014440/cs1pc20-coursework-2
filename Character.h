#pragma once
#include <iostream>
#include <vector>
#include "Item.h"

using namespace std;

class Character {
private:
    string name;
    int health;
    vector<Item> Inventory;
public:
    Character(const string& name, int health);
    void TakeDamage(int damage);
    void StoreItem(Item item);
    void DeleteItem(Item::itemTypes iType);
    bool CheckForItem(Item::itemTypes iType);
    int GetHealth();
};