#include "CommandReader.h"
#include <iostream>
#include "Player.h"
#include "CommandReader.h"


CommandReader::CommandReader(Player* player) {
	_player = player;
}
void CommandReader::readCommand(const string& command) {
	if (command == "help") {
		cout << "Commands: \n";
		cout << "move 'direction' (e.g move south) : moves player to the room in that direction if exists\n";
		cout << "look around : view a list of items if any that are in the room.\n";
		cout << "inspect 'item' : gives an item's description.\n";
		cout << "quests : view list of current quests\n";
		cout << "quit : quits program\n";
	}
	else if (command.find("move", 0, 4) != string::npos) {
		_player->move(command);
	}
	else if (command.find("pickup", 0, 6) != string::npos) {
		string item = command.substr(7);
		// item.erase(remove_if(item.begin(), item.end(), isspace), item.end());
		for (int i = 0; i < item.size(); i++) {
			item[i] = tolower(item[i]);
		}
		_player->PickupItem(item);
	}
	else if (command.find("inspect", 0, 7) != string::npos) {
		string item = command.substr(8);
		for (int i = 0; i < item.size(); i++) {
			item[i] = tolower(item[i]);
		}
		_player->InspectItem(item);
	}
	else if (command.find("quest") != string::npos) {
		for (auto i : _player->currentQuests) {
			cout << i->questDesc << " : " << i->GetStatus() << "\n";
		}
	}
	else if (command.find("look around") != string::npos) {
		if (_player->GetLocation()->GetItems().size() == 0) {
			cout << "No items in this room.\n";
			return;
		}
		cout << "Items in room: \n";
		for (auto i : _player->GetLocation()->GetItems()) {
			cout << i.GetName() << "\n";
		}
	}
	else {
		cout << "Invalid command, type help to see a list of available commands.";
	}
}
