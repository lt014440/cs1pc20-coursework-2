#include <iostream>
#include "Player.h"
#pragma once

class CommandReader
{
	private:
		Player* _player;
	public:
		CommandReader(Player* player);
		void readCommand(const string& command);
};

