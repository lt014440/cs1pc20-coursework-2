#include <iostream>
#pragma once

using namespace std;

class Quest
{
public:
	string questDesc;
	enum Status { NotStarted, InProgress, Completed };
	Status currentStatus = NotStarted;

	virtual ~Quest() {};

	void startQuest();
	void updateQuest();
	void completeQuest();
	string GetStatus();
};

