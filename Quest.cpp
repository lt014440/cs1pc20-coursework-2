#include "Quest.h"

/*
Quest::Quest(string desc, Status status) {
	questDesc = desc;
	currentStatus = status;
}
*/
void Quest::startQuest() {
	currentStatus = InProgress;
}
void Quest::updateQuest() {
	int intValue = static_cast<int>(currentStatus);
	if (intValue < Completed)
		currentStatus = static_cast<Status>(intValue);
}
void Quest::completeQuest() {
	currentStatus = Completed;
}
string Quest::GetStatus() {
	if (currentStatus == 0) {
		return "Not Started";
	}
	else if (currentStatus == 1) {
		return "In Progress";
	}
	else {
		return "Completed";
	}
}