#include <iostream>
#include <fstream>
#include <sstream>
#include "Arena.h"
#include "Room.h"

using namespace std;

void Arena::AddRoom(string& name, Room* room) {
    rooms[name] = room;
}
Room* Arena::GetRoom(const string name) {
    auto it = rooms.find(name);
    if (it != rooms.end()) {
        return (Room*)it->second;
    }
    return nullptr; // no room found
}
void Arena::ConnectRooms(const string room1, const string room2, const string direction) {
    if (GetRoom(room1) != nullptr && GetRoom(room2) != nullptr) {
        // Check if the rooms exist before accessing them
        if (GetRoom(room1)->HasExit(direction)) {
            cout << "Room" << room1 << "already has a connection in that direction." << endl;
        }
        else {
            // Connect the rooms if they exist and there is no existing connection
            GetRoom(room1)->AddExit(direction, GetRoom(room2));
            GetRoom(room2)->AddExit(getOppositeDirection(direction), GetRoom(room1));
            // cout << "Rooms connected successfully." << endl;
        }
    }
    else {
        cout << "One or both of the rooms do not exist." << endl;
    }
}
void Arena::LoadMapFromFile(const string& filename, int& roomsCounter) {
    ifstream file(filename);

    if (!file.is_open()) {
        cerr << "Error opening file: " << filename << endl;
        return;
    }

    string line;
    while (getline(file, line)) {
        int newRoom = 0;
        // Use stringstream to split the line into tokens
        if (line.empty()) return;
        stringstream ss(line);
        string token;

        // Read the room names and connection direction from each line
        string room1, room2, direction, roomLocked, pass;
        getline(ss, room1, '|');
        getline(ss, room2, '|');
        getline(ss, direction, '|');
        getline(ss, roomLocked, '|');
        getline(ss, pass);

        // Create or retrieve rooms and connect them
        Room* r1 = GetRoom(room1);
        if (r1 == nullptr) {
            newRoom++;
            r1 = new Room(room1);
            AddRoom(room1, r1);
        }

        if (roomLocked == "true") {
            cout << "new lock\n";
            int iC = 0;
            int randIC = rand() % rooms.size();
            int randICNote = rand() % rooms.size();
            for (auto i : rooms) {
                for (auto j : get<Room*>(i)->GetItems()) {
                    if (j.GetName() == "key") continue;
                }
                if (iC == randIC) {
                    if (pass == " ") {
                        Item key("Key", "just a key", Item::key);
                        get<Room*>(i)->AddItem(key);
                    }
                    else {
                        if (pass == "tree") {
                            Item painting("Painting", "////////////////////////////////////////////*************,******//////////////////////////((((((((((\n/((((((//////////////*********/*/**/////*******,,,,***********//////////////////////////////((((((((\n*/////////*******************,,,/,**///**,,,**,**,**********//////////////////////////////////////((\n*,,,,,,,,*********,,,,,,,.,,*,,,*//(%%%%%%%%%&&&#/#*//******,**///*****/////////////////////////////\n,,,,,,,,,,,,,,,,,,,,,./..***//##(/(#%%&&&%%&&&&&&@@@&(((/,,,,.,..,,,*********,,*,*****//////////////\n,,,,.........,,,,,,,...*//(#%#%%&&&%(/(%%%&%&&&&@@@@@&&%&&##%,.,.,,,,,,,,,,,,,,,,,,,,,,*************\n  .... ............ /,***(/##(%*/*/(((##%&&&&&&&&&@@&@&&@&@&@&&%*,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,\n          ....../*/((#%%##((#&(/((#%%%%%&&%%%%&&@@@@@@@@@@@@@@@#//*,,,............,...,,,,,,,,,,,,,,\n           .../../**,*//////(##@%#%%&&&@&@@@&%&%%%%%%&&&&&@@@@@&&%#/,.........,,,,,,,,,,,,,,,,,*,***\n           ....  ,//#&&@####/@#(#@&&&&&&&&@%&&@@@@@@@@@@@@&&&&&&%#,,............,,,,,,,,,,,,,*******\n       ..     . *(/#((((#%####%%%@%@(&@@&%&@@@@@@@@@@@@@@@@@@@&&&&%%*.*................,,,,,,,,,,,,,\n                ,,./((###%%%((#%&%&&&%%%#%&&&&@@@@@@@&@@@@@@@@@@@&&%,,,..................,,,,,,,,,,,\n                   , ,,,./(%#/#%@@@@@@@@@@@@@@@@@@@@@@@&&%%%&&@&@@&%#/,......................,,,,,,,\n                        ,/(((%&&%%(#%%&%%&&@&@@@@@@&@@@@@&&&&&&%#//,,,..............................\n                       ***/.*//.,/##%%&&&@&@&@%(/.(,@@(##&%%#(((,,,,.................,..............\n                            . , /##%%.//(###/,.@@@&.&@.....,........................................\n                                      ..*.       .%%@@...............................,,,,,,,,,,,*///\n///***,,,,,,,..       .........                   %@@    ......................,,,,*********///((##%\n###################(((//***,,,,...................(&@@............,,,,,***********/////(############\n#%%%%%%%%%%%%%%%%%%%%%#####..*#%,,,................*@@@......,,,,,*********///((((((((((((((########\n#####%%%#####(((((((/((//(,((#&&&,...,,,,,,,,,,**/((@@@///*,,,,,.,,,,,,..,,,,***.*///((((###########\n(((((((///***********..,,,*(%&&@@@@&&&%%%%##(((%%&&&&&&@@@@@&&%%%#######(((((((((((((((((###((((((/(\n#((/*/((((((%%./***//((((/*//////(##%#(%%&&&&&&&%##(////(#%%%%%&&@@@&@&&&&&&&&&&%%%%%%%((/%%%###%%%%\n%%%&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%&%&&%%%%%%%%%&&&&&&&%%%((#%%%%%%%###%%%&&&&&&&&&&&&&%%%%%%&&&&&%%\n&&&&&&&&&&&&&&&&&&&%&&&&&&&&&&&&&&&&&&%&&##%%%%%%%%%%%&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n&&&&&&&&&&&@@@@@@@&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&@&&@\n&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&&@@@&&&&&&&&&&&&&&&&&&&&&&&&@@@@@@@@@@@@@@", Item::painting);
                            get<Room*>(i)->AddItem(painting);
                        }
                        randIC = -1;
                    }
                    if (randICNote == -1)
                        break;
                }
                if (iC == randICNote && pass != " ") {
                    Item note("Note", "Hint for the locked room: inspect the painting\n", Item::painting);
                    get<Room*>(i)->AddItem(note);
                    randICNote = -1;
                    if (randIC == -1)
                        break;
                }
                iC++;
            }
        }

        Room* r2 = GetRoom(room2);
        if (r2 == nullptr) {
            newRoom++;
            r2 = new Room(room2);
            AddRoom(room2, r2);
            r2->SetPass(pass);
        }

        if (roomLocked == "true") {
            r2->ChangeDoorLock(true);
        }
        else {
            r2->ChangeDoorLock(false);
        }

        // cout << newRoom;
        roomsCounter += newRoom;

        ConnectRooms(room1, room2, direction);
        if (file.peek() == EOF) {
            // No more lines in the file
            break;
        }
    }
    file.close();
}
string Arena::getOppositeDirection(const string& direction) {
    if (direction == "north") {
        return "south";
    }
    else if (direction == "south") {
        return "north";
    }
    else if (direction == "east") {
        return "west";
    }
    else if (direction == "west") {
        return "east";
    }
    // Handle other cases or return an appropriate default
    return "unknown_direction";
}