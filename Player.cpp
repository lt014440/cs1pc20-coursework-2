#include "Player.h"
#include <iostream>
#include <vector>
#include "Room.h"
#include "Character.h"
#include "RoomsQuest.h"
#pragma once

using namespace std;

Player::Player(const string& setname, int sethealth) : Character(setname, sethealth) {
    // Additional initialization for the Player class if needed
}
void Player::SetLocation(Room* room) {
	for (Quest* i : currentQuests) {
		if (RoomsQuest* derivedQuest = dynamic_cast<RoomsQuest*>(i)) {
			derivedQuest->RoomsUpdate(room);
		}
	}
    location = room;
};
Room* Player::GetLocation() {
    return location;
};

void Player::PickupItem(string& item) {
	for (auto i : Player::GetLocation()->GetItems()) {
		string newName = "";
		for (int x = 0; x < i.GetName().size(); x++) {
			newName += tolower(i.GetName()[x]);
		}
		if (newName == item) {
			if (i.itemType != Item::painting) {
				cout << "Picked up " << item << "\n";
				Player::StoreItem(i);
				Player::GetLocation()->RemoveItem(newName);
			}
			else {
				cout << "Cannot pickup painting.\n";
			}
		}
	}
}

void Player::InspectItem(string& item) {
	bool found = false;
	for (auto i : Player::GetLocation()->GetItems()) {
		string newName = "";
		for (int x = 0; x < i.GetName().size(); x++) {
			newName += tolower(i.GetName()[x]);
		}
		if (newName == item) {
			found = true;
			cout << i.GetDesc() << endl;
		}
	}
	if (!found) {
		cout << "No item with that name.\n";
	}
}

void Player::move(const string& command) {
	string dir = command.substr(4, 9);
	dir.erase(remove_if(dir.begin(), dir.end(), isspace));
	if (dir.find("north") != string::npos || dir.find("east") != string::npos || dir.find("south") != string::npos || dir.find("west") != string::npos) {
		if (Player::GetLocation()->HasExit(dir)) {
			if (Player::GetLocation()->GetExit(dir)->IsLocked()) {
				cout << "Room is locked.\n";
				if (Player::GetLocation()->GetExit(dir)->CheckPass(" ")) {
					if (Player::CheckForItem(Item::key)) {
						cout << "You have a key, would you like to unlock it?\n";
						while (true) {
							cout << ">> ";
							string choice;
							cin >> choice;
							if (choice == "yes") {
								Player::GetLocation()->GetExit(dir)->ChangeDoorLock(false);
								Player::DeleteItem(Item::key);
								cout << "Used key from inventory.\n";
								Player::SetLocation(Player::GetLocation()->GetExit(dir));
								break;
							}
							else if (choice == "no") {
								cout << "What will you do next?\n";
								break;
							}
							else {
								cout << "Not a valid option, please type 'yes' or 'no'.\n";
							}
						}
						cin.clear();
						cin.ignore(numeric_limits<streamsize>::max(), '\n');
					}
					else {
						cout << "You do not have a key, you cannot go this way.\n";
					}
				}
				else {
					cout << "The door has a password, what do you enter?\n";
					while (true) {
						cout << ">> ";
						string choice;
						cin >> choice;
						if (choice == Player::GetLocation()->GetExit(dir)->GetPass()) {
							Player::GetLocation()->GetExit(dir)->ChangeDoorLock(false);
							Player::SetLocation(Player::GetLocation()->GetExit(dir));
							break;
						}
						else {
							cout << "Incorrect password, maybe there's a hint around somewhere.\n";
							break;
						}
					}
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
				}
			}
			else {
				Player::SetLocation(Player::GetLocation()->GetExit(dir));
			}
			
		}
		else {
			cout << "No exit that way, try a different way.";
		}
	}
	else {
		cout << "Type where you would like to move to as well.";
	}
}