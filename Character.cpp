#include <iostream>
#include <vector>
#include "Item.h"
#include "Character.h"

using namespace std;

Character::Character(const string& setname, int sethealth) {
    name = setname;
    health = sethealth;
}
void Character::TakeDamage(int damage) {
    health -= damage;
};
int Character::GetHealth() {
    return health;
}
void Character::StoreItem(Item item) {
    Inventory.push_back(item);
}

bool Character::CheckForItem(Item::itemTypes iType) {
    for (auto i : Inventory)
    {
        if (i.itemType == iType) {
            return true;
        }
    }
    return false;
}

void Character::DeleteItem(Item::itemTypes iType) {
    auto i = Inventory.begin();
    while (i != Inventory.end()) {
        if (i->itemType == iType) {
            i = Inventory.erase(i);
        }
        else
            ++i;
    }
}
