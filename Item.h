#include <iostream>
#include <string>
#include <map>
#include <vector>
#pragma once

using namespace std;

class Item {
    private:
        string name;
        string desc;
    public:
        enum itemTypes { box, key, painting };
        itemTypes itemType;
        Item(const string& setname, const string& setdesc, itemTypes iType);
        string GetName();
        string GetDesc();
};