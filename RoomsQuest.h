#include <iostream>
#include "Quest.h"
#include <vector>
#include "Room.h"
#pragma once

using namespace std;

class RoomsQuest : public Quest
{
public:
	RoomsQuest(string setdesc, Status setstatus, Room* startRoom, int roomsC);
	int roomsVisited = 1;
	int roomsCount;
	vector<Room*> roomsVector;

	void RoomsUpdate(Room* roomToAdd);
};

