#include <iostream>
#include <vector>
#include <map>
#include "Item.h"
#pragma once

using namespace std;

class Room {
    private:
        string desc;
        map<string, const Room*> exits;
        vector<Item> items;
        bool isLocked;
        string pass;
    public:
        Room(const string& desc);
        string GetDescription();
        void AddItem(const Item& item);
        void RemoveItem(string item);
        void AddExit(string direction, const Room* room);
        bool HasExit(const string& direction) const;
        Room* GetExit(const string& direction) const;
        bool IsLocked();
        vector<Item> GetItems();
        void ChangeDoorLock(bool condition);
        bool CheckPass(string guess);
        void SetPass(string password);
        string GetPass();
};