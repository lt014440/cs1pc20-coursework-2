#include <iostream>
#include "RoomsQuest.h"
#include "Arena.h"

using namespace std;

RoomsQuest::RoomsQuest(string setdesc, Status setstatus, Room* startRoom, int roomsC) {
	questDesc = setdesc;
	currentStatus = setstatus;
	roomsCount = roomsC;
	roomsVector.push_back(startRoom);
}

void RoomsQuest::RoomsUpdate(Room* roomToAdd) {
	for (auto i : roomsVector) {
		if (i == roomToAdd) {
			cout << "I think I've here before\n";
			return;
		}
	}
	roomsVector.push_back(roomToAdd);
	roomsVisited++;
	if (roomsVisited == roomsCount) {
		currentStatus = Completed;
	}
}