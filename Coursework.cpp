#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "Item.h"
#include "Room.h"
#include "Character.h"
#include "Player.h"
#include "Arena.h"
#include "CommandReader.h"
#include "RoomsQuest.h"
#include <cstdlib> 
#include <ctime>

using namespace std;

int main() {
    srand((unsigned)time(0));
    // initialize objects
    Player player1("Alex", 100);
    CommandReader reader(&player1);
    //Room startRoom("starting room");
    //Room room2("deadend");
    //Item key("dIndad 21", "just a key", Item::key);

    // setup starting room
    //startRoom.AddItem(box);
    //startRoom.AddExit("south", &room2);

    // setup deadend room
    //room2.AddExit("north", &startRoom);
    int roomsCounter = 0;
    Arena arena;
    arena.LoadMapFromFile("map.txt", roomsCounter);
    // cout << roomsCounter;
    // arena.GetRoom("startRoom")->AddItem(key);
    player1.SetLocation(arena.GetRoom("startRoom"));
    // Quest allRoomsQuest("Visit all rooms", Quest::InProgress);
    RoomsQuest allRoomsQuest("Visit all rooms", Quest::InProgress, arena.GetRoom("startRoom"), roomsCounter);
    player1.currentQuests.push_back(&allRoomsQuest);

    reader.readCommand("help");
    // game loop
    while (true) {
        cout << "Current Location: " << player1.GetLocation()->GetDescription() << "\n";

        string command;
        cout << "> ";
        getline(cin, command);
        if (command == "quit" || command == "exit") {
            break;
        }
        reader.readCommand(command);

        /* OLD OPTIONS FROM OLDER LABS

        // options
        cout << "Options: \n";
        cout << "1. Look around | \n";
        cout << "2. Interact with an item | \n";
        cout << "3. Move to another room | \n";
        cout << "4. Quit" << std::endl;

        int choice;
        cin >> choice;
        if (std::cin.fail()) {
            std::cout << "That's not a number.  Try again.\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        if (choice == 1) {
            cout << "You look around the room and find these items:\n";
            for (Item& item : player1.GetLocation()->GetItems()) {
                cout << item.GetName();
            }
        }
        else if (choice == 2) {
            cout << "Enter the name of the item you want to interact with: ";
            string itemName;
            cin >> itemName;
            for (Item& item : player1.GetLocation()->GetItems()) {
                if (item.GetName() == itemName) {
                    cout << "Put " << itemName << " in inventory\n";
                    player1.StoreItem(item);
                    break;
                }
            }
        }
        else if (choice == 3) {
            cout << "Enter direction ('south', 'west', etc): \n";
            string direction;
            cin >> direction;
            Room* nextRoom = player1.GetLocation()->GetExit(direction);
            if (nextRoom != nullptr) {
                player1.SetLocation(nextRoom);
                cout << "You moved to the " << direction << "." << endl;
            }
            else {
                cout << "There is no exit in that direction. Try a different direction." << endl;
            }
        }
        else if (choice == 4) {
            cout << "Goodbye! \n";
            break;
        }
        else {
            cout << "Number not in range, try again.\n";
            continue;
        }
        */
    }

    return 0;
}