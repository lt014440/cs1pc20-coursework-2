#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "Item.h"

using namespace std;

string Item::GetName() {
    return name;
};
string Item::GetDesc() {
    return desc;
}
Item::Item(const string& setname, const string& setdesc, itemTypes iType) {
    name = setname;
    desc = setdesc;
    itemType = iType;
};